import nltk
import pandas as pd
import numpy as np

# If you would like to work with the raw text you can use 'moby_raw'
with open('moby.txt', 'r') as f:
    moby_raw = f.read()

# If you would like to work with the novel in nltk.Text format you can use 'text1'
moby_tokens = nltk.word_tokenize(moby_raw)
print("n_tokens = {}".format(len(moby_tokens)))

text1 = nltk.Text(moby_tokens)
print('type of text1 i s{}'.format(type(text1)))

# Compute the word frquency
dist = nltk.FreqDist(moby_tokens)
print("the type of dist is {}".format(type(dist)))

def answer_eight():
    dict_postag = {}
    sents = nltk.sent_tokenize(moby_raw)
    for sent in sents:
        leafs = nltk.pos_tag(sent)
        for leaf in leafs:
            if dict_postag.get(leaf[0]) is None:
                dict_postag[leaf[1]] = 1
            else:
                dict_postag[leaf[1]] += 1

    # Find the top five most frequent tags
    tuples = sorted(dict_postag.items(), key=lambda x: x[1], reverse=True)
    top_5 = tuples[:5]
    return top_5


answer_eight()