import re
import datetime
import numpy as np

def parse_date_string_numerical(ptn, str):
    match = re.search(ptn, str)
    if match is None:
        # str_norm = "01011970"
        return None # Need this return so next pattern can be tried

    else:
        str_1 = match.group(1)
        str_2 = match.group(2)
        str_3 = match.group(3)

        # Determine year
        if len(str_3) == 2:
            str_3 = "19" + str_3

        # Standardize str_2 and str_1 for month and day
        if len(str_1) == 1:
            str_1 = '0' + str_1
        elif len(str_2) == 1:
            str_2 = '0' + str_2

        # Determine month and day
        if (int(str_1) <= 12 and int(str_2) <= 12):
            str_norm = str_1 + str_2 + str_3
        elif (int(str_1) > 12 and int(str_2) <= 12):
            str_norm = str_2 + str_1 + str_3
        elif (int(str_1) <= 12 and int(str_2) > 12):
            str_norm = str_1 + str_2 + str_3
        else:
            str_norm = "01011970"

        return str_norm

def parse_date_string_with_string_month_first(ptn, str):
    dict_month = {"jan":"01", "feb":"02", "mar":"03", "apr":"04", "may":"05", "jun":"06",
                  "jul":"07", "aug":"08", "sep":"09", "oct":"10", "nov":"11", "dec":"12"}

    match = re.search(ptn, str)
    if match is None:
        # str_norm = "01011970"
        return None # Need this so next pattern can be tried

    else:
        str_1 = match.group(1)
        str_2 = match.group(2)
        str_3 = match.group(3)

        # Determine year - we assume year is always the last group among the three
        if len(str_3) == 2:
            str_3 = "19" + str_3

        # Standardize str_2
        elif len(str_2) == 1:
            str_2 = '0' + str_2

        # str1 represents month, let's standardize it as a three-letter string, and then convert it to
        # numerical representation
        str_1 = str_1.lower()
        str_1 = str_1[:3]

        # Look up the numerical representation
        str_1 = dict_month.get(str_1, None)
        if (str_1 is None):
            str_norm = None
        else:
            str_norm = str_1+str_2+str_3;

        return str_norm


def parse_date_string_with_string_month_second(ptn, str):
        dict_month = {"jan": "01", "feb": "02", "mar": "03", "apr": "04", "may": "05", "jun": "06",
                      "jul": "07", "aug": "08", "sep": "09", "oct": "10", "nov": "11", "dec": "12"}

        match = re.search(ptn, str)
        if match is None:
            # str_norm = "01011970"
            return None  # Need this so next pattern can be tried

        else:
            str_1 = match.group(1)
            str_2 = match.group(2)
            str_3 = match.group(3)

            # Determine year - we assume year is always the last group among the three
            if len(str_3) == 2:
                str_3 = "19" + str_3
            elif len(str_3) == 1:
                str_3 = "1970"

            # Standardize str_1 that represent days
            elif len(str_1) == 1:
                str_1 = '0' + str_1

            # str_2 represents month, let's standardize it as a three-letter string, and then convert it to
            # numerical representation
            str_2 = str_2.lower()
            str_2 = str_2[:3]

            # Look up the numerical representation
            str_2 = dict_month.get(str_2, None)
            if (str_2 is None):
                str_norm = None
            else:
                str_norm = str_2 + str_1 + str_3

            return str_norm


def parse_date_string_with_month_year(ptn, str):
    """
    Parse date string with numerical month and year separated by / e.g. 6/2017
    :param ptn: Regex pattern
    :param str: The string to be parsed
    :return:  The normalized date string
    """
    match = re.search(ptn, str)
    if match is None:
        # str_norm = "01011970"
        return None  # Need this return so next pattern can be tried

    else:
        str_1 = match.group(1)
        str_2 = match.group(2)

        # Standardize str_1
        if len(str_1) == 1:
            str_1 = '0' + str_1

        if int(str_1) > 12:
            str_norm = None
        else:
            str_norm = str_1 + "01" + str_2;

        return str_norm


def parse_date_string_with_string_month_year(ptn, str):
    dict_month = {"jan": "01", "feb": "02", "mar": "03", "apr": "04", "may": "05", "jun": "06",
                  "jul": "07", "aug": "08", "sep": "09", "oct": "10", "nov": "11", "dec": "12"}

    match = re.search(ptn, str)
    if match is None:
        # str_norm = "01011970"
        return None  # Need this so next pattern can be tried

    else:
        str_1 = match.group(1)
        str_2 = match.group(2)

        # str_1 represents month, let's standardize it as a three-letter string, and then convert it to
        # numerical representation
        str_1 = str_1.lower()
        str_1 = str_1[:3]

        # Look up the numerical representation
        str_1 = dict_month.get(str_1, None)
        if (str_1 is None):
            str_norm = None
        else:
            str_norm = str_1 + "01" + str_2

        return str_norm

def parse_date_string_with_year_only(ptn, str):
        """
        Parse date string with numerical month and year separated by / e.g. 6/2017
        :param ptn: Regex pattern
        :param str: The string to be parsed
        :return:  The normalized date string
        """
        match = re.search(ptn, str)
        if match is None:
            # str_norm = "01011970"
            return None  # Need this return so next pattern can be tried

        else:
            str_1 = match.group(1)
            str_norm = "01"+ "01" + str_1;
            return str_norm


# Define the regular expressions for possible date strings
ptns = []
ptns.append(r'(\d{1,2})[/-](\d{1,2})[/-](\d{2,4})') #patns[0], e.g. 10/23/2013
ptns.append(r'(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(?:\w*|\.)(?:\s+|-)(\d{1,2})(?:rd|st|th|nd)?(?:,\s*|\s+|-)(\d{2,4})') #patns[1], e.g. Oct 23, 2013
ptns.append(r'(\d{1,2})\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(?:\w*|\.)(?:,\s+|\s+)(\d{2,4})') #patns[2], e.g. 23 Oct 2013
ptns.append(r'(\d{1,2})/(\d{2,4})') #patns[3], e.g. 6/2008
ptns.append(r'(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w*\s+(\d{4})') #patns[4], e.g. Feb 2009
ptns.append(r'(\d{4})') #patns[5], e.g. 2008

date_list = ['04/20/2009', '04/20/09', '4/20/09', '4/3/09'] # patns[0]
date_list.extend(['Mar-20-2009', 'Mar 20, 2009', 'March 20, 2009', 'Mar. 20, 2009', 'Mar 20 2009']) # ptns[1]
date_list.extend(['20 Mar 2009', '20 March 2009', '20 Mar. 2009', '20 March, 2009']) #ptns[2]
date_list.extend(['Mar 20th, 2009', 'Mar 21st, 2009', 'Mar 22nd, 2009'])
date_list.extend(["6/1025", "Oct 2013", "2018"])
dates = []
str_norm = ''

# First round parse date strings with all parts numerical
pat = ptns[0]
for str in date_list:
    str_norm = parse_date_string_numerical(pat, str)
    if str_norm is None:
        dates.append(None)
    else:
        dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
        dates.append(dt.date())

# Second round parse date strings with months represented by strings, e.g. Oct, January, etc, and the
# month string is the first substring of the whole date string
pat = ptns[1]
for i in range(len(date_list)):
    str = date_list[i]
    if dates[i] is None:
        str_norm = parse_date_string_with_string_month_first(pat, str)
        if str_norm is not None:
            dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
            dates[i] = dt.date()

# Third round parse date strings with months represented by strings, e.g. Oct, January, etc, and the
# month string is the second substring of the whole date string
pat = ptns[2]
for i in range(len(date_list)):
    str = date_list[i]
    if dates[i] is None:
        str_norm = parse_date_string_with_string_month_second(pat, str)
        if str_norm is not None:
            dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
            dates[i] = dt.date()

# Fourth round parse date strings with months represented by strings, e.g. Oct, January, etc, and the
# whole date string only contains month followed by year, e.g. Feb 2013
pat = ptns[3]
for i in range(len(date_list)):
    str = date_list[i]
    if dates[i] is None:
        str_norm = parse_date_string_with_month_year(pat, str)
        if str_norm is not None:
            dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
            dates[i] = dt.date()

# Fifth round parse date strings with months represented by strings, e.g. Oct, January, etc, and the
# whole date string only contains numerical month followed by year, e.g. 6/2013
pat = ptns[4]
for i in range(len(date_list)):
    str = date_list[i]
    if dates[i] is None:
        str_norm = parse_date_string_with_string_month_year(pat, str)
        if str_norm is not None:
            dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
            dates[i] = dt.date()

# Sixth round parse date strings with only year, e.g. 2013
pat = ptns[5]
for i in range(len(date_list)):
    str = date_list[i]
    if dates[i] is None:
        str_norm = parse_date_string_with_year_only(pat, str)
        if str_norm is not None:
            dt = datetime.datetime.strptime(str_norm, "%m%d%Y")
            dates[i] = dt.date()

dates = sorted(dates)
print(dates)
