import re

## search method: match = re.search(pat, str)
def search(str, pat):
    match = re.search(pat, str)
    if match:
        print('found', match.group())
    else:
        print('did not find')
    return match
"""
search(str = 'an example word:cat!', pat = r'word:\w\w\w' )
search(str = 'piiij', pat = r'iii')
search(str = 'piiij', pat = r'igs')
search(str = 'piiig', pat = r'..g') # Wild card
search(str = 'p123g',  pat = r'\d\d\d')
search(str = '@@abcd', pat = r'\w\w\w')

## Seacrh repetion pattern *, +, ?
search(str = "piiig", pat = r'pi+')
search(str = "xx1 2   3xx", pat = r'\d\s*\d\s*\d')
search(str = "xx12  3xx",   pat = r'\d\s*\d\s*\d')

## Start and end of a string
search(str = 'foobar', pat = r'^b\w+')
search(str = 'foobar', pat = r'b\w+')

## Email recongnition
str = "purple alice-b@google.com monkey dishwasher"
search(str, r'\w+[a-zA-Z0-9-]*@\w+[a-zA-Z0-9_]*\.\w\w+')
match = search(str, r'(\w+[a-zA-Z0-9-]*)@(\w+[a-zA-Z0-9_]*\.\w\w+)')
print(match.group(1))
print(match.group(2))
"""

## findall
str = 'purple alice@google.com, blah monkey bob@abc.com blah dishwasher'
emails = re.findall(r'(\w+[a-zA-Z0-9-]*)@(\w+[a-zA-Z0-9_]*\.\w\w+)', str)
print(emails)
for email in emails:
    print(email)

## Substitution re.replace(pat, replacement, str)
str = 'purple alice@google.com, blah monkey bob@abc.com blah dishwasher'
str = re.sub(r'(\w+[a-zA-Z0-9-]*)@(\w+[a-zA-Z0-9_]*\.\w\w+)', r'\1@yo-yo-dyne.com', str)
print(str)