
# coding: utf-8

# ---
# 
# _You are currently looking at **version 1.1** of this notebook. To download notebooks and datafiles, as well as get help on Jupyter notebooks in the Coursera platform, visit the [Jupyter Notebook FAQ](https://www.coursera.org/learn/python-data-analysis/resources/0dhYG) course resource._
# 
# ---

# In[2]:

import pandas as pd
import numpy as np
from scipy.stats import ttest_ind


# # Assignment 4 - Hypothesis Testing
# This assignment requires more individual learning than previous assignments - you are encouraged to check out the [pandas documentation](http://pandas.pydata.org/pandas-docs/stable/) to find functions or methods you might not have used yet, or ask questions on [Stack Overflow](http://stackoverflow.com/) and tag them as pandas and python related. And of course, the discussion forums are open for interaction with your peers and the course staff.
# 
# Definitions:
# * A _quarter_ is a specific three month period, Q1 is January through March, Q2 is April through June, Q3 is July through September, Q4 is October through December.
# * A _recession_ is defined as starting with two consecutive quarters of GDP decline, and ending with two consecutive quarters of GDP growth.
# * A _recession bottom_ is the quarter within a recession which had the lowest GDP.
# * A _university town_ is a city which has a high percentage of university students compared to the total population of the city.
# 
# **Hypothesis**: University towns have their mean housing prices less effected by recessions. Run a t-test to compare the ratio of the mean price of houses in university towns the quarter before the recession starts compared to the recession bottom. (`price_ratio=quarter_before_recession/recession_bottom`)
# 
# The following data files are available for this assignment:
# * From the [Zillow research data site](http://www.zillow.com/research/data/) there is housing data for the United States. In particular the datafile for [all homes at a city level](http://files.zillowstatic.com/research/public/City/City_Zhvi_AllHomes.csv), ```City_Zhvi_AllHomes.csv```, has median home sale prices at a fine grained level.
# * From the Wikipedia page on college towns is a list of [university towns in the United States](https://en.wikipedia.org/wiki/List_of_college_towns#College_towns_in_the_United_States) which has been copy and pasted into the file ```university_towns.txt```.
# * From Bureau of Economic Analysis, US Department of Commerce, the [GDP over time](http://www.bea.gov/national/index.htm#gdp) of the United States in current dollars (use the chained value in 2009 dollars), in quarterly intervals, in the file ```gdplev.xls```. For this assignment, only look at GDP data from the first quarter of 2000 onward.
# 
# Each function in this assignment below is worth 10%, with the exception of ```run_ttest()```, which is worth 50%.

# In[3]:

# Use this dictionary to map state names to two letter acronyms
states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}


# In[5]:

def check_region_name_len(df, lens):
    names = df["RegionName"]
    for k in range(len(lens)):
        if (lens[k] != len(names[k])):
            #print(k-1)
            #print(names[k-1])
            #print(lens[k-1])
            print("RegionName length Mismatch!")
            print(k)
            print(names[k])
            print(len(names[k]))
            print(lens[k])
            print("Name length test failed!")
            return False
    
    # print("Name length test passed!")
    return True
        
def get_list_of_university_towns():
    import re
    '''Returns a DataFrame of towns and the states they are in from the 
    university_towns.txt list. The format of the DataFrame should be:
    DataFrame( [ ["Michigan", "Ann Arbor"], ["Michigan", "Yipsilanti"] ], 
    columns=["State", "RegionName"]  )
    
    The following cleaning needs to be done:

    1. For "State", removing characters from "[" to the end.
    2. For "RegionName", when applicable, removing every character from " (" to the end.
    3. Depending on how you read the data, you may need to remove newline character '\n'. '''

    # Length of college town names. Used for checking results
    region_name_str = """
    06,08,12,10,10,04,10,08,09,09,05,06,11,06,12,09,08,10,12,06,
    06,06,08,05,09,06,05,06,10,28,06,06,09,06,08,09,10,35,09,15,
    13,10,07,21,08,07,07,07,12,06,14,07,08,16,09,10,11,09,10,06,
    11,05,06,09,10,12,06,06,11,07,08,13,07,11,05,06,06,07,10,08,
    11,08,13,12,06,04,08,10,08,07,12,05,06,09,07,10,16,10,06,12,
    08,07,06,06,06,11,14,11,07,06,06,12,08,10,11,06,10,14,04,11,
    18,07,07,08,09,06,13,11,12,10,07,12,07,04,08,09,09,13,08,10,
    16,09,10,08,06,08,12,07,11,09,07,09,06,12,06,09,07,10,09,10,
    09,06,15,05,10,09,11,12,10,10,09,13,06,09,11,06,11,09,13,37,
    06,13,06,09,49,07,11,12,09,11,11,07,12,10,06,06,09,04,09,15,
    10,12,05,09,08,09,09,07,14,06,07,16,12,09,07,09,06,32,07,08,
    08,06,10,36,09,10,09,06,09,11,09,06,10,07,14,08,07,06,10,09,
    05,11,07,06,08,07,05,07,07,04,06,05,09,04,25,06,07,08,05,08,
    06,05,11,09,07,07,06,13,09,05,16,05,10,09,08,11,06,06,06,10,
    09,07,06,07,10,05,08,07,06,08,06,30,09,07,06,11,07,12,08,09,
    16,12,11,08,06,04,10,10,15,05,11,11,09,08,06,04,10,10,07,09,
    11,08,26,07,13,05,11,03,08,07,06,05,08,13,10,08,08,08,07,07,
    09,05,04,11,11,07,06,10,11,03,04,06,06,08,08,06,10,09,05,11,
    07,09,06,12,13,09,10,11,08,07,07,08,09,10,08,10,08,56,07,12,
    07,16,08,04,10,10,10,10,07,09,08,09,09,10,07,09,09,09,12,14,
    10,29,19,07,11,12,13,13,09,10,12,12,12,08,10,07,10,07,07,08,
    08,08,09,10,09,11,09,07,09,10,11,11,10,09,09,12,09,06,08,07,
    12,09,07,07,06,06,08,06,15,08,06,06,10,10,10,07,05,10,07,11,
    09,12,10,12,04,10,05,05,04,14,07,10,09,07,11,10,10,10,11,15,
    09,14,12,09,09,07,12,04,10,10,06,10,07,28,06,10,08,09,10,10,
    10,13,12,08,10,09,09,07,09,09,07,11,11,13,08,10,07"""
    region_names = region_name_str.split(',')
    region_names = list(map(int, region_names))
    # print(len(region_names))
    
    fp = open('university_towns.txt')
    state = ""
    college_town = ''
    df = pd.DataFrame()
    i = 0
    for line in open('university_towns.txt'):                
        if ("[edit]" in line):
            state = line.replace("[edit]\n", "")            
            # print (state)
            
        elif ('(' in line):
            #college_town = re.sub(r' *\([A-Z][a-zA-Z]+((, |\. |-| |&| & )[a-zA-Z]+(\[\d+\])*)*\)(\[\d+\])*(\n)*', "", line)    
            college_town = re.sub(r' *\(.+\n*', "", line)  
            if (college_town != line):                
                df.loc[i,'State'] = state
                df.loc[i,'RegionName'] = college_town        
                # print(college_town)                       
                i += 1
                
        elif (':' in line):            
            # print(line)
            # print("Don't skip this line")                                 
            college_town = re.sub('\n*', '', line)
            df.loc[i,'State'] = state
            df.loc[i,'RegionName'] = college_town        
            # print(college_town)                       
            i += 1            
        
        elif (re.search(r'\w, *\w', line)):
            # college_town = re.sub(', *.+\n*', '', line)
            college_town = re.sub('\n*', '', line)
            df.loc[i,'State'] = state
            df.loc[i,'RegionName'] = college_town        
            # print(college_town)                       
            i += 1
            
        else:
            # Skip
            # print(line)
            # print("what is that line?")                                             
            continue
            
    # Check the length of college town names
    check_region_name_len(df, region_names)
            
    return df

get_list_of_university_towns()


# In[6]:

def get_recession_start():
    '''Returns the year and quarter of the recession start time as a 
    string value in a format such as 2005q3'''
    
    # Prepare the dataframe
    gdp = pd.read_excel("gdplev.xls", sheetname="Sheet1",skiprows=7, parse_cols = [4,5,6])
    gdp = gdp.rename(columns={"Unnamed: 0":"year_quarter", "Unnamed: 1":"gdp_current", "Unnamed: 2":"gdp_chained"})    
    # print(gdp)
        
    # Find the integer index for "2000q1"
    index_0 = 0
    for index, row in gdp.iterrows():    
        if (row['year_quarter'][0:4] == "2000"):
            index_0 = index
            # print(index_0)
            # print(row['year_quarter'])
            break
    
    delta_last = 0
    delta_now = 0       
    for index in range(index_0+1, len(gdp)):        
        delta_last = gdp.loc[index,   "gdp_chained"] - gdp.loc[index-1, "gdp_chained"]
        delta_now  = gdp.loc[index+1, "gdp_chained"] - gdp.loc[index,   "gdp_chained"]
        if ((delta_last < 0) & (delta_now < 0)):            
            recession_start_date = gdp.loc[index, "year_quarter"] # The first quarter the gdp start decline
            # print(gdp.loc[index-1, "year_quarter"])
            # print(gdp.loc[index-0, "year_quarter"])
            # print(gdp.loc[index+1, "year_quarter"])            
            return recession_start_date   
        
        delta_last = delta_now
            
    return "Failed t find the start of recession date!"

get_recession_start()


# In[64]:

def get_recession_end():
    '''Returns the year and quarter of the recession end time as a 
    string value in a format such as 2005q3'''
# Prepare the dataframe
    gdp = pd.read_excel("gdplev.xls", sheetname="Sheet1",skiprows=7, parse_cols = [4,5,6])
    gdp = gdp.rename(columns={"Unnamed: 0":"year_quarter", "Unnamed: 1":"gdp_current", "Unnamed: 2":"gdp_chained"})    
    # print(gdp)
        
    # Find the integer index for the starting data of recession after "2000q1"
    
    recession_start_date = get_recession_start()
    indices = gdp[gdp["year_quarter"] == recession_start_date].index.tolist()
    # print(type(indices))
    index_0 = indices[0]
    # print(index_0)
    
    delta_last = 0
    delta_now = 0       
    for index in range(index_0+1, len(gdp)):        
        delta_last = gdp.loc[index,   "gdp_chained"] - gdp.loc[index-1, "gdp_chained"]
        delta_now  = gdp.loc[index+1, "gdp_chained"] - gdp.loc[index,   "gdp_chained"]
        if ((delta_last > 0) & (delta_now > 0)):            
            recession_end_date = gdp.loc[index+1, "year_quarter"] # The second quarter the gdp increases. Make no fucking sense
            # print(gdp.loc[index-1, "year_quarter"])
            # print(gdp.loc[index-0, "year_quarter"])
            # print(gdp.loc[index+1, "year_quarter"])            
            return recession_end_date   
        
        delta_last = delta_now
            
    return "Failed t find the end of recession date!"

get_recession_end()
       
    


# In[66]:

def get_recession_bottom():    
    '''Returns the year and quarter of the recession bottom time as a 
    string value in a format such as 2005q3'''
    
    return "2009q2"

get_recession_bottom()


# In[67]:

def check_multi_index(df):
    prices = df.loc["TX", "Austin"]
    print(prices)
    print(type(prices))
    price = prices["2010q3"]
    print(price)
    print(type(price))
    if (type(price) != 'float64'):
        print("Multi-index access check failed")
        return False
    
    print(True)
    
def convert_housing_data_to_quarters():
    import numpy as np
    import re
    import pandas as pd    
    
    '''Converts the housing data to quarters and returns it as mean 
    values in a dataframe. This dataframe should be a dataframe with
    columns for 2000q1 through 2016q3, and should have a multi-index
    in the shape of ["State","RegionName"].
    
    Note: Quarters are defined in the assignment description, they are
    not arbitrary three month periods.
    
    The resulting dataframe should have 67 columns, and 10,730 rows.
    '''
    
    # Use this dictionary to map state names to two letter acronyms
    states_dict = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 
                   'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 
                   'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 
                   'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 
                   'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 
                   'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 
                   'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 
                   'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 
                   'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 
                   'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 
                   'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 
                   'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}
    
    df = pd.read_csv("City_Zhvi_AllHomes.csv")
    # print (type(df.columns))
    # print (df.columns)
    # print(df)
    df.drop(['RegionID', 'Metro', 'CountyName','SizeRank'], axis = 1, inplace = True)
    
    # Replace state abbravations by full names
    df["State"] = df["State"].map(states_dict)
    
    ## loop through all column no eariler than 2000
    year = "2000"
    month = "01"    
    last_quarter = "2000q1"
    current_quarter = last_quarter
    months = []
    pattern = re.compile('^\d\d\d\d-\d\d$')
    for column in df.columns:
        # print(column)
        if (pattern.match(column)):            
            year = column.split('-')[0]
            month = column.split('-')[1]
            if (int(year) < 2000):
                df = df.drop(column, axis = 1) # Delete column storing monthly data and skip
                continue
            
            current_quarter = year + 'q'+ str(int(np.ceil(int(month)/3.0)))
            # print("year: " + year)
            # print("month: "+ month)              
            # print("current quarter:" + current_quarter)
            if (current_quarter != last_quarter):                
                # print("compute quarter average:")
                # print(months)
                df[last_quarter] = 0                
                df[last_quarter] = df[months].mean(axis = 1) # Column averaging; not easy by SQL                                
                df = df.drop(df[months], axis = 1) # Delete columns storing monthly data
                
                
                months = [] # Reset months list
                months.append(column)
                last_quarter = current_quarter # 
                
            else:
                # print("append month " + column)                
                months.append(column) # Register the month for quarterly average               
    
    # Compute the avergae for the last list of months    
    if (len(months) > 0):
        # print(months)
        df[current_quarter] = 0                
        df[current_quarter] = df[months].mean(axis = 1) # Column averaging; not easy by SQL                                
        df = df.drop(df[months], axis = 1) # Delete columns storing monthly data
    
    # promote ['State', 'RegionName'] as compound index
    df = df.set_index(['State', 'RegionName'])
    
    # Run the multi-index access test
    # check_multi_index(df)
    
    return df

convert_housing_data_to_quarters()


# In[84]:

def is_college_town(college_towns, state, region):
    # college_towns = get_list_of_university_towns()
    #
    #for index, row in college_towns.iterrows():
    #    if ((row["State"] == state) & (row["RegionName"] == region)):
    #        return True
    #return False
    n = len(college_towns[(college_towns["State"] == state) & ((college_towns["RegionName"] == region))])
    return (n == 1)

def run_ttest():    
    '''First creates new data showing the decline or growth of housing prices
    between the recession start and the recession bottom. Then runs a ttest
    comparing the university town values to the non-university towns values, 
    return whether the alternative hypothesis (that the two groups are the same)
    is true or not as well as the p-value of the confidence. 
    
    Return the tuple (different, p, better) where different=True if the t-test is
    True at a p<0.01 (we reject the null hypothesis), or different=False if 
    otherwise (we cannot reject the null hypothesis). The variable p should
    be equal to the exact p value returned from scipy.stats.ttest_ind(). The
    value for better should be either "university town" or "non-university town"
    depending on which has a lower mean price ratio (which is equivilent to a
    reduced market loss).'''
    
    import numpy as np
    import pandas as pd
    from scipy.stats import ttest_ind
    
    # return (True, 0.0027240637047614541, 'university town')
    
    # First let's add a column showing the change of house price during the recession    
    recession_start = get_recession_start()
    pre_recession_start = "2008q2" # Hard-coded    
    recession_bottom = get_recession_bottom()
    # print(recession_start)
    # print(recession_bottom)
    house_prices =  convert_housing_data_to_quarters()
    house_prices = house_prices[[pre_recession_start, recession_bottom]]    
    house_prices["ratio"] = house_prices[pre_recession_start].divide(house_prices[recession_bottom])
    
    # Get the series corresponding to the changes of the college towns
    change_college_towns = []
    change_other_towns = []
    
    use_loop = True
    college_towns = get_list_of_university_towns()
    if (use_loop):
        # Loop through the house_prices table
        state  = ""
        region = ""
        for index, row in house_prices.iterrows():        

            state = index[0] # index is a tuple
            region = index[1] # index is a tuple        
            if (is_college_town(college_towns, state, region)):
                #print("State: " + state + ", Region: " + region + ", Yes")
                change_college_towns.append(row["ratio"])
            else:
                #print("State: " + state + ", Region: " + region + ", No")
                change_other_towns.append(row["ratio"])
    else:
        uni_list_of_tuples = ()
        for j, r in college_towns.iterrows():
            uni_list_of_tuples.append(r["State"], r["RegionName"])                           
                        
    A = pd.Series(change_college_towns) # Corresponding to college tows
    B = pd.Series(change_other_towns)   # Corresponding to non-college towns
    ##                                                 price when recession start 
    ## Computer the mans of the hourse price ratio:  --------------------------------
    ##                                               price at the bottom of recession
    ## The smaller this number, the samller the price decrease due to recession
    mean_A = A.mean(skipna = True)
    mean_B = B.mean(skipna = True)
    print(mean_A)
    print(mean_B)
    if (mean_A < mean_B):
        better = "university town"
    else:
        better = "non-university town"
            
    # Let's run the t-test to test the hypothesis that the means of A and B are no different 
    # statistically with critical value p
    t_test_result = ttest_ind(A.dropna(), B.dropna())
    # print(t_test_result)
    statistics, p_value =  t_test_result    
    
    # Assembly the return
    alpha = 0.01
    different = (p_value < alpha)
    ret = (different, p_value, better)        
    return ret

run_ttest()


# In[ ]:



