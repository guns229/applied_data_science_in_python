
# coding: utf-8

# ---
# 
# _You are currently looking at **version 1.2** of this notebook. To download notebooks and datafiles, as well as get help on Jupyter notebooks in the Coursera platform, visit the [Jupyter Notebook FAQ](https://www.coursera.org/learn/python-data-analysis/resources/0dhYG) course resource._
# 
# ---

# # Assignment 2 - Pandas Introduction
# All questions are weighted the same in this assignment.
# ## Part 1
# The following code loads the olympics dataset (olympics.csv), which was derrived from the Wikipedia entry on [All Time Olympic Games Medals](https://en.wikipedia.org/wiki/All-time_Olympic_Games_medal_table), and does some basic data cleaning. 
# 
# The columns are organized as # of Summer games, Summer medals, # of Winter games, Winter medals, total # number of games, total # of medals. Use this dataset to answer the questions below.

# In[4]:

import pandas as pd

df = pd.read_csv('olympics.csv', index_col=0, skiprows=1)

for col in df.columns:
    if col[:2]=='01':
        df.rename(columns={col:'Gold'+col[4:]}, inplace=True)
    if col[:2]=='02':
        df.rename(columns={col:'Silver'+col[4:]}, inplace=True)
    if col[:2]=='03':
        df.rename(columns={col:'Bronze'+col[4:]}, inplace=True)
    if col[:1]=='№':
        df.rename(columns={col:'#'+col[1:]}, inplace=True)

names_ids = df.index.str.split('\s\(') # split the index by '('

df.index = names_ids.str[0] # the [0] element is the country name (new index) 
df['ID'] = names_ids.str[1].str[:3] # the [1] element is the abbreviation or ID (take first 3 characters from that)

df = df.drop('Totals')
df.head()


# ### Question 0 (Example)
# 
# What is the first country in df?
# 
# *This function should return a Series.*

# In[5]:

# You should write your whole answer within the function provided. The autograder will call
# this function and compare the return value against the correct solution value
def answer_zero():
    # This function returns the row for Afghanistan, which is a Series object. The assignment
    # question description will tell you the general format the autograder is expecting
    return df.iloc[0]

# You can examine what your function returns by calling it in the cell. If you have questions
# about the assignment formats, check out the discussion forums for any FAQs
answer_zero().name 


# ### Question 1
# Which country has won the most gold medals in summer games?
# 
# *This function should return a single string value.*

# In[6]:

def answer_one():
    ## Extract the gold column
    counts = df['Gold'] 
    # print(type(counts))
    
    ## Sort the column ascendingly
    counts = counts.order()
    # print(counts)
    
    ## Return the country that won the most golds
    max_count_nation = counts.index[-1]
    # print(max_count_nation)
    
    return max_count_nation
    
answer_one()


# ### Question 2
# Which country had the biggest difference between their summer and winter gold medal counts?
# 
# *This function should return a single string value.*

# In[7]:

def answer_two():
    
    ## Create a column for medal difference
    df["diff"] = df["Gold"] - df["Gold.1"]
    # print(df["diff"])
    
    ## Sort the column ascendingly
    col = df["diff"].order()
    # print(col)
    
    ## Return the country with the biggest difference
    max_diff_nation = col.index[-1]
    # print(max_diff_nation)
    
    return max_diff_nation    

answer_two()


# Question 3
# Which country has the biggest difference between their summer gold medal counts and winter gold medal counts relative to their total gold medal count? 
# 
# $$\frac{Summer~Gold - Winter~Gold}{Total~Gold}$$
# 
# Only include countries that have won at least 1 gold in both summer and winter.
# 
# *This function should return a single string value.*

# In[13]:

def answer_three():
    
    ## Get the tuples for countries that have won at least one gold in both summer and winter games
    df2 = df[(df["Gold"] >= 1) & (df["Gold.1"] >= 1)]
             
    ## Create a column for medal difference
    df2["rel_diff"] = (df2["Gold"] - df2["Gold.1"])/df2["Gold.2"]
    # print(df2["rel_diff"])
    
    ## Sort the column ascendingly
    col = df2["rel_diff"].order()
    # print(col)
    
    ## Return the country with the biggest difference
    max_rel_diff_nation = col.index[-1]
    # print(max_diff_nation)
    
    return max_rel_diff_nation

answer_three()


# ### Question 4
# Write a function that creates a Series called "Points" which is a weighted value where each gold medal (`Gold.2`) counts for 3 points, silver medals (`Silver.2`) for 2 points, and bronze medals (`Bronze.2`) for 1 point. The function should return only the column (a Series object) which you created.
# 
# *This function should return a Series named `Points` of length 146*

# In[9]:

def answer_four():
    # Create a new column for the weithed score
    df["Weighted"] = 3.0*df["Gold.2"] + 2.0*df["Silver.2"] + df["Bronze.2"]
    n = df["Weighted"].size
    # print(n)
    
    ## Create a series named points
    s = pd.Series()
    s = df["Weighted"]
    s = s.rename("points")
    return s

answer_four()


# ## Part 2
# For the next set of questions, we will be using census data from the [United States Census Bureau](http://www.census.gov/popest/data/counties/totals/2015/CO-EST2015-alldata.html). Counties are political and geographic subdivisions of states in the United States. This dataset contains population data for counties and states in the US from 2010 to 2015. [See this document](http://www.census.gov/popest/data/counties/totals/2015/files/CO-EST2015-alldata.pdf) for a description of the variable names.
# 
# The census dataset (census.csv) should be loaded as census_df. Answer questions using this as appropriate.
# 
# ### Question 5
# Which state has the most counties in it? (hint: consider the sumlevel key carefully! You'll need this for future questions too...)
# 
# *This function should return a single string value.*

# In[10]:

import pandas as pd
census_df = pd.read_csv('census.csv')


# In[12]:

def answer_five():
    ## Filter out data with SUMLEVEL = 40 - meaning the data is for state
    df = census_df[census_df["SUMLEV"] != 40]
    
    ## Construct unique list, i.e. a set of states included in df
    states = df["STNAME"].unique()
    # print(type(states))
    
    ## Count the number of counties for each state    
    max_count = -1
    i_max = -1
    i = 0
    for state in states:
        
        ## Loop through the rows of df
        count = 0;
        for index, row in df.iterrows():
            if (row["STNAME"] == state):
                count += 1        
        if (count > max_count):
            max_count = count
            
            i_max = i
        i += 1
               
    return states[i_max]

answer_five()


# ### Question 6
# Only looking at the three most populous counties for each state, what are the three most populous states (in order of highest population to lowest population)? Use `CENSUS2010POP`.
# 
# *This function should return a list of string values.*

# In[56]:

def answer_six():
    ## Filter out data with SUMLEVEL = 40 - meaning the data is for state
    df = census_df[census_df["SUMLEV"] != 40]
    
    ## Construct unique list, i.e. a set of states included in df
    states = df["STNAME"].unique()
    # print(type(states))
    
    ## Compile a list of state population based on the three most populus counties of a state
    populations_df = pd.DataFrame()
    populations_df["state"]= states
    populations_df["population_estimate"] = 0
    populations_df = populations_df.set_index("state")
    
    for state in states:
        
        ## Loop through the rows of df        
        state_county_populations = []
        for index, row in df.iterrows():
            if (row["STNAME"] == state):
                state_county_populations.append(row["CENSUS2010POP"])        
        
        ## Sort state_county_populations inplace        
        state_county_populations.sort(reverse=True)
        # print(state)
        # print(state_county_populations)  
        n = len(state_county_populations)
        if (n > 3):
            n = 3
        
        sum_top_3 = 0
        for i in range(n):
            sum_top_3 += state_county_populations[i]        
        populations_df.loc[state, "population_estimate"] = sum_top_3
        
    sorted_df = populations_df.sort("population_estimate", ascending = False)            
    lst = []
    lst.append(sorted_df.index[0])
    lst.append(sorted_df.index[1])
    lst.append(sorted_df.index[2])
    # print(type(lst))
    return lst

answer_six()


# ### Question 7
# Which county has had the largest absolute change in population within the period 2010-2015? (Hint: population values are stored in columns POPESTIMATE2010 through POPESTIMATE2015, you need to consider all six columns.)
# 
# e.g. If County Population in the 5 year period is 100, 120, 80, 105, 100, 130, then its largest change in the period would be |130-80| = 50.
# 
# *This function should return a single string value.*

# In[57]:

def answer_seven():
     ## Filter out data with SUMLEVEL = 40 - meaning the data is for state
    df = census_df[census_df["SUMLEV"] != 40]
        
    ## Add a max_diff column
    df["MAXDIFF"] = 0    
        
    ## Loop through all rows of df    
    i_max = 0
    max_diff = -1
    i = 0
    for index, row in df.iterrows():        
        min_pop = 1000000000
        max_pop = -1
        if (row["POPESTIMATE2010"] < min_pop):
            min_pop = row["POPESTIMATE2010"]
        if (row["POPESTIMATE2010"] > max_pop):
            max_pop = row["POPESTIMATE2010"]
            
        if (row["POPESTIMATE2011"] < min_pop):
            min_pop = row["POPESTIMATE2011"]
        if (row["POPESTIMATE2011"] > max_pop):
            max_pop = row["POPESTIMATE2011"]
            
        if (row["POPESTIMATE2012"] < min_pop):
            min_pop = row["POPESTIMATE2012"]
        if (row["POPESTIMATE2012"] > max_pop):
            max_pop = row["POPESTIMATE2012"]
            
        if (row["POPESTIMATE2013"] < min_pop):
            min_pop = row["POPESTIMATE2013"]
        if (row["POPESTIMATE2013"] > max_pop):
            max_pop = row["POPESTIMATE2013"]
            
        if (row["POPESTIMATE2014"] < min_pop):
            min_pop = row["POPESTIMATE2014"]
        if (row["POPESTIMATE2014"] > max_pop):
            max_pop = row["POPESTIMATE2014"]
            
        if (row["POPESTIMATE2015"] < min_pop):
            min_pop = row["POPESTIMATE2015"]
        if (row["POPESTIMATE2015"] > max_pop):
            max_pop = row["POPESTIMATE2015"]
        
        diff = max_pop - min_pop
        row["MAXDIFF"] = diff
        if (diff > max_diff):
            max_diff = diff
            i_max = i
        i += 1
        # print (max_diff)
        
    #print(df)        
    #print(df.iloc[i_max]["STNAME"])
    #print(df.iloc[i_max]["CTYNAME"])
    return df.iloc[i_max]["CTYNAME"]
    
answer_seven()
    


# ### Question 8
# In this datafile, the United States is broken up into four regions using the "REGION" column. 
# 
# Create a query that finds the counties that belong to regions 1 or 2, whose name starts with 'Washington', and whose POPESTIMATE2015 was greater than their POPESTIMATE 2014.
# 
# *This function should return a 5x2 DataFrame with the columns = ['STNAME', 'CTYNAME'] and the same index ID as the census_df (sorted ascending by index).*

# In[14]:

def answer_eight():
     ## Filter out data with SUMLEVEL = 40 - meaning the data is for state
    df = census_df[census_df["SUMLEV"] != 40]
    
    df2 = pd.DataFrame()
    df2["STNAME"]  = ''
    df2['CTYNAME'] = ''
    
    for index, row in df.iterrows():
        if (((row["REGION"] == 1) | (row["REGION"] == 2)) & (row["CTYNAME"].split(' ')[0] == "Washington") & (row["POPESTIMATE2015"] > row["POPESTIMATE2014"])):
            df2.loc[index] = [row["STNAME"],row["CTYNAME"]]                                   
            
    return df2
    
answer_eight()


# In[ ]:



